import React, { Component } from 'react';
import { BrowserRouter,Redirect, Route } from 'react-router-dom';
import toastr from 'toastr';
import { VerticalSideBar, Tasks } from './components'
import { TasksPanel } from './components/menu';
import 'semantic-ui-css/semantic.min.css';
import './styles/App.css';
import './styles/toastr.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      tasks: {
        1: {name: "TestA", status: "success", description: "Test",
          last_execution_time: Date.now()}, 
        2: {name: "TestB", status: "failed", description: "Test",
          last_execution_time:Date.now()},
        3: {name: "TestC", status: "inactive", description: "Test",
          last_execution_time: Date.now(), },
        4: {name: "TestD", status: "running", description: "Test",
          last_execution_time: Date.now()},
        5: {name: "TestE", status: "failed", description: "Test", 
          last_execution_time: Date.now()},
        6: {name: "TestF", status: "success", description: "Test",
          last_execution_time: Date.now()}
      }
    }
    this.addTask = this.addTask.bind(this);
    this.updateTask = this.updateTask.bind(this);
  }

  addTask(newTask) {
    const newTaskID = Date.now();
    const updatedTasks = {...this.state.tasks, [newTaskID]: newTask }
    this.setState({
      tasks: updatedTasks
    });

    toastr.success('You can access it on a Tasks page','Task ' + newTask.name+ ' has been created!');
    return <Redirect to='/tasks'/>;
  }

  updateTask(taskKey, task) {
    const updatedTasks = {...this.state.tasks, [taskKey]: task}
    this.setState({
      tasks: updatedTasks
    });
    toastr.success('You can access it on a Tasks page','Task has been updated');
    return <Redirect to='/tasks'/>;
  }

  render() {
    const { tasks } = this.state;

    const Home = () => (
      <div>
        <h2>Home</h2>
      </div>
    )

    return (
      <div className="App">
        <BrowserRouter>
          <div>
            <VerticalSideBar>
              <TasksPanel tasks={tasks} />
            </VerticalSideBar>
            <div className="task-container">
                <div>
                  <Route exact path="/" component={Home}/>
                  <Route path="/tasks" render= {(props) =>
                    <Tasks {...props} updateTask={this.updateTask} addTask={this.addTask} tasks={tasks} />
                  }/>
                </div>
           </div>
          </div>
        </BrowserRouter>
      </div>
   );
  }
}

export default App;
