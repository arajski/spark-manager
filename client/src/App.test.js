import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';
import Enzyme, { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from './App';
import { VerticalSideBar, Task, Tasks } from './components'
import { TasksPanel } from './components/menu';

Enzyme.configure({ adapter: new Adapter() });
const tasks = {
  1: {name: "TestA", status: "success", description: "Test",
    last_execution_time: Date.now()}, 
  2: {name: "TestB", status: "failed", description: "Test",
    last_execution_time:Date.now()},
  3: {name: "TestC", status: "inactive", description: "Test",
    last_execution_time: Date.now(), },
  4: {name: "TestD", status: "running", description: "Test",
    last_execution_time: Date.now()},
  5: {name: "TestE", status: "failed", description: "Test", 
    last_execution_time: Date.now()}
} 

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});

describe('VerticalSideBar', ()=> {
  it('renders', ()=> {
    const div = document.createElement('div');
    ReactDOM.render(<VerticalSideBar tasks={tasks} />, div);
  });
  it('matches snapshot', ()=> {
    const component = renderer.create(<VerticalSideBar tasks={tasks} />);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});

describe('TasksPanel', ()=> {
  it('renders',()=> {
    const div = document.createElement('div');
    ReactDOM.render(<TasksPanel tasks={tasks} />, div);
  });
  it('calculates running tasks properly', ()=> {
    const panel = shallow(<TasksPanel tasks={tasks} />);
    const taskCount = panel.state().count;
    expect(taskCount['running']).toBe(1);
  });
  it('calculates sucessful tasks properly', ()=> {
    const panel = shallow(<TasksPanel tasks={tasks} />);
    const taskCount = panel.state().count;
    expect(taskCount['success']).toBe(1);
  });
  it('calculates failed tasks properly', ()=> {
    const panel = shallow(<TasksPanel tasks={tasks} />);
    const taskCount = panel.state().count;
    expect(taskCount['failed']).toBe(2);
  });
  it('calculates inactive tasks properly', ()=> {
    const panel = shallow(<TasksPanel tasks={tasks} />);
    const taskCount = panel.state().count;
    expect(taskCount['inactive']).toBe(1);
  });
  it('reacts to changes in task list', ()=> {
    let tasksToUpdate = {...tasks}
    const panel = shallow(<TasksPanel tasks={tasksToUpdate} />);
    const newTask = {name: "TestA", status: "success", description: "Test",
    last_execution_time: Date.now()}

    tasksToUpdate = {...tasksToUpdate, 5: newTask };
    panel.setProps({tasks: tasksToUpdate});
    const taskCount = panel.state().count;
    expect(taskCount['success']).toBe(2);
  });
});

describe('Task', ()=> {
  it('renders', ()=> {
    const div = document.createElement('div');
    ReactDOM.render(<Task task={tasks['1']}/>, div);
  });
  it('matches snapshot', ()=> {
    const component = renderer.create(<Task task={tasks['1']} />);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
