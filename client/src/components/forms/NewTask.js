import React, { Component } from 'react';
import { Form, Button } from 'semantic-ui-react';
import { Redirect } from 'react-router-dom';

class NewTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      redirect: false
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ 
      [event.target.name]: event.target.value 
    });
  }

  handleSubmit() {
    const newTask = {
      name: this.state.name,
      description: this.state.description,
      status: 'inactive',
      last_execution_time: null
    }
    this.props.addTask(newTask);
    this.setState({
      redirect: true
    });
  }

  render(){
    const { name, description } = this.state;
    
    if (this.state.redirect) {
      return <Redirect to='/tasks' />
    }

    return(
      <div class='task-form'>
        <h2>Create New Task</h2>
        <Form onSubmit={this.handleSubmit}>
            <Form.Input label="Name" 
                        value={name} 
                        name='name' 
                        onChange={this.handleChange} 
                        placeholder="Title" />
            <Form.TextArea label="Description" 
                           value={description} 
                           name='description' 
                           onChange={this.handleChange} 
                           placeholder="Description..." />
            <Button color='teal' type='submit'>Create</Button>
        </Form>
      </div> 
    )
  }
}
export default NewTask;
