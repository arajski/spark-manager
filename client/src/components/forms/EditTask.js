import React, { Component } from 'react';
import { Form, Button } from 'semantic-ui-react';
import { Redirect } from 'react-router-dom';

class EditTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      redirect: false
    }
    this.setInitialValues = this.setInitialValues.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillMount(){ 
    this.setInitialValues(this.props);
  }

  componentWillReceiveProps(nextProps){
    this.setInitialValues(nextProps);
  }

  setInitialValues(props){
    this.setState({
      name: props.task.name,
      description: props.task.description
    })
  }

  handleChange(event) {
    this.setState({ 
      [event.target.name]: event.target.value 
    });
  }

  handleSubmit() {
    const updatedTask = {...this.props.task, name: this.state.name, description: this.state.description}
    this.props.updateTask(this.props.taskKey, updatedTask);
    this.setState({
      redirect: true
    });
  }

  render(){
    const { name,description } = this.state;
   
    if (this.state.redirect) {
      return <Redirect to='/tasks' />
    }

    return(
      <div class='task-form'>
        <h2>Edit Task</h2>
        <Form onSubmit={this.handleSubmit}>
            <Form.Input label="Name" 
                        value={name} 
                        name='name' 
                        onChange={this.handleChange} 
                        placeholder="Title" />
            <Form.TextArea label="Description" 
                           value={description} 
                           name='description' 
                           onChange={this.handleChange} 
                           placeholder="Description..." />
            <Button color='blue' type='submit'>Update</Button>
        </Form>
      </div> 
    )
  }
}
export default EditTask;
