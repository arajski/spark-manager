import React, { Component } from 'react';
import { Menu, Sidebar, Header, Icon } from 'semantic-ui-react';

class VerticalSideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeItem: this.props.activeItem
    }
    this.handleItemClick = this.handleItemClick.bind(this);
  }

  handleItemClick(e, { name }){
    this.setState({ 
      activeItem: name    
    })
  }

  render(){
    const { activeItem } = this.state;
    const { handleItemClick } = this;

    return(
      <Sidebar as={Menu} visible={true} className="sidebar" width="thin" vertical>  
        <Menu.Item>
          <Header as='h4' icon textAlign="center">
            <Icon name='fire' circular />
            Spark Manager
          </Header>
        </Menu.Item> 
        <Menu.Item>
          <Menu.Header color='violet'>
            <Icon name='home' /><span> Home</span>
          </Menu.Header>
          <Menu.Menu>
            <Menu.Item name='profile' active={activeItem === 'profile'} onClick={handleItemClick} />
            <Menu.Item name='settings' active={activeItem === 'settings'} onClick={handleItemClick} />
            <Menu.Item name='help' active={activeItem === 'help'} onClick={handleItemClick} />
            <Menu.Item name='logout' active={activeItem === 'logout'} onClick={handleItemClick} />
          </Menu.Menu>
        </Menu.Item>
        {
          React.Children.map(this.props.children, function(child) {
            return React.cloneElement(child, {activeItem: activeItem, handleItemClick: handleItemClick});
          })
        }
       </Sidebar>
    );
  }
}
export default VerticalSideBar
