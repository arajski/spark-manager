import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import { Grid, Menu, Button } from 'semantic-ui-react';
import Task from './Task';
import NewTask from './forms/NewTask';
import EditTask from './forms/EditTask';

class Tasks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      statusFilter: 'all',
      nameFilter: ''
    }
    this.returnForm = this.returnForm.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
  }

  componentDidMount() {
    if(this.props.location.state) {
      this.setState({
        statusFilter: this.props.location.state.statusFilter
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps !== this.props && nextProps.location.state) {
      this.setState({
        statusFilter: nextProps.location.state.statusFilter
      })
    }
  }

  returnForm(id) {
    const { tasks, addTask, updateTask } = this.props;

    if(id in tasks) {
      return(
        <EditTask taskKey={id} task={tasks[id]} updateTask={updateTask} />    
      )
    }
    else {
      return(
        <NewTask addTask={addTask} />
      )
    }
  }
  
  handleSearchChange(event) {
    this.setState({
      nameFilter: event.target.value
    })
  }

  render() {
    const { tasks } = this.props;
    const { statusFilter, nameFilter } = this.state;
    const taskKeys = Object.keys(tasks);

    return(
      <div>        
        <Menu secondary>
          <Menu.Item>
            <Link to={`/tasks/new`}>
              <Button content='New' icon='empty star' labelPosition='left' color='teal' />
            </Link>
          </Menu.Item>
          <Menu.Menu position='right'>
            <div className='ui right aligned category search item'>
              <div className='ui transparent icon input'>
                <input className='prompt' type='text' placeholder='Search Task...' onChange={this.handleSearchChange}/>
                <i className='search link icon' />
              </div>
            </div>
          </Menu.Menu>
        </Menu>

        <Route path={`/tasks/:id`} render={({match}) => (
              <div>
              {
                this.returnForm(match.params.id)
              }
              </div>
        )} />
   
        <Grid container columns={3} doubling stackable>
            {
              taskKeys.filter(key => tasks[key].status === statusFilter || statusFilter === 'all')
                      .filter(key => tasks[key].name.includes(nameFilter))
                      .map(key => <Task key={key} taskKey={key} task={tasks[key]} />)
            }
            </Grid>
      </div> 
    )
  }
}
export default Tasks
