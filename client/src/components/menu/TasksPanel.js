import React, { Component } from 'react';
import { Menu, Icon, Label, Progress } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

class TasksPanel extends Component {  
  constructor(props) {
    super(props);
    this.state = {
      count: []
    }
    this.calculateTaskTypes = this.calculateTaskTypes.bind(this);
  }
 
  componentDidMount(){
    this.setState({
      count: this.calculateTaskTypes(this.props.tasks)
    })
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps !== this.props) {
      this.setState({
        count: this.calculateTaskTypes(nextProps.tasks)
      }); 
    }
  }

  calculateTaskTypes(tasks) {
    const count = Object.keys(tasks).reduce((total, key) => {
      total[tasks[key].status] = (total[tasks[key].status] || 0) + 1;
      return total;
    }, {});
    return count; 
  }
 
  render(){
    const { count } = this.state;
    const { handleItemClick, activeItem, tasks } = this.props;
 
    return(
      <div>
        <Menu.Item>
          <Menu.Header><Icon name='cubes' /><span> Tasks</span></Menu.Header>
          <Menu.Menu>
            <Link to={{pathname: '/tasks', state: {statusFilter: 'all'}}}>
              <Menu.Item name='all tasks' active={activeItem === 'all tasks'} onClick={handleItemClick} />
            </Link>
            <Link to={{pathname: '/tasks', state: {statusFilter: 'success'}}}>
              <Menu.Item name='completed' active={activeItem === 'completed'} onClick={handleItemClick} >
                <Label size='mini' color='olive'>{count["success"]}</Label>Completed
              </Menu.Item>
            </Link>
            <Link to={{pathname: '/tasks', state: {statusFilter: 'failed'}}}>
              <Menu.Item name='failed' active={activeItem === 'failed'} onClick={handleItemClick} >
                <Label size='mini' color='red'>{count["failed"]}</Label>Failed
              </Menu.Item>
            </Link>
            <Link to={{pathname: '/tasks', state: {statusFilter: 'running'}}}>
              <Menu.Item name='running' active={activeItem === 'running'} onClick={handleItemClick} >
                <Label size='mini' color="blue">{count["running"]}</Label>Running
              </Menu.Item>
            </Link>
          </Menu.Menu>
        </Menu.Item>
        <Menu.Item>
          <Menu.Header><Icon name='tasks' /><span> Jobs</span></Menu.Header>
          <Menu.Menu>
          {
            Object.keys(tasks).map(key => 
              tasks[key].status === "running" ?
              <Menu.Item key={key}>
                <Progress percent={100} color="blue" size="tiny" active>
                  {tasks[key].name}
                </Progress>
              </Menu.Item>
            : null )
          }
          </Menu.Menu>
        </Menu.Item>
      </div>
    );
  }
}
export default TasksPanel;
