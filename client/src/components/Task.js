import React, {Component} from 'react';
import { Grid, Segment, Header, Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

class Task extends Component {
  constructor(props){
    super(props);
  }

  render(){
    const { taskKey, task } = this.props;
    
    return (
      <Grid.Column>
        <Segment.Group className={"segment-"+task.status}>
          <Segment>
            <Header as={"h4"} textAlign="left">
              {task.name}
            </Header>
          </Segment>
          <Segment>
            {task.description}
          </Segment>
          <Segment textAlign="left">
            <Link to={`/tasks/${taskKey}`}>
              <Button color='blue' size='mini' icon='settings' labelPosition='left' content="Edit" />
            </Link>
          </Segment>
          </Segment.Group> 
      </Grid.Column>
    )
  }
}
export default Task
