#!/bin/sh
if [ ! -f server ]; then 
  echo "Compiling server..."
  go build server.go
fi

echo "Starting servers..."
nohup ./server > server.log 2>&1 &
cd client && nohup npm start > client.log 2>&1 &

